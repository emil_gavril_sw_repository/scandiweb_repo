<?php
namespace public_html\Model\ProductTypes;
    
    
    
    class CompactDiskProduct extends \public_html\Model\AbstractProduct
    {
        
        protected $id;
        protected $sku;
        protected $name;
        protected $price;
        
        private $size;
        
        
        
        public function __construct( $compactdisk_meta_arr )
        {
            
            $this->setBaseAttributes( $compactdisk_meta_arr );
            $this->setTypeSpecificAttributes( $compactdisk_meta_arr );
            
        }
        
        protected function getTypeSpecificAttributes()
        {
            try{
                
                $type_meta_arr = [
                
                "Label"     =>  "Size",
                
                "Quantity"  =>  $this->size,
                
                "Unit"      =>  "MB"
                
                ];
                
            }catch(Exception $e){
                
                echo "ERROR: Type specific attribute extraction failed" . $e->getMessage() . "\n";
                
                return FALSE;
                
            }
            
            return $type_meta_arr;
            
        }
        
        protected function setTypeSpecificAttributes( $type_meta_arr )
        {
            
            try{
                
                if( array_key_exists( "Quantity", $type_meta_arr ) )//Information to be stored comes from DB
                {
                    
                    $this->size = $type_meta_arr["Quantity"];
                    
                }
                else//Information to be stored comes from Add Product form
                {
                    
                    $this->size = $type_meta_arr["Size"];
                    
                }
                
            }catch( Exception $e ){
                
                echo "ERROR: Type specific attribute assignment failed\n" . $e->getMessage() . "\n";
                
                return FALSE;
                
            }
            
            return TRUE;
            
        }
        
        
    }

?>