<?php
namespace public_html\Model\ProductTypes;
    
    
    
    class BookProduct extends \public_html\Model\AbstractProduct
    {
        
        protected $id;
        protected $sku;
        protected $name;
        protected $price;
        
        private $weight;
        
        
        
        public function __construct( $book_meta_arr )
        {
            
            $this->setBaseAttributes( $book_meta_arr );
            $this->setTypeSpecificAttributes( $book_meta_arr );
            
        }
        
        protected function getTypeSpecificAttributes()
        {
            try{
                
                $type_meta_arr = [
                
                "Label"     =>  "Weight",
                
                "Quantity"  =>  $this->weight,
                
                "Unit"      =>  "KG"
                
                ];
                
            }catch(Exception $e){
                
                echo "ERROR: Type specific attribute extraction failed" . $e->getMessage() . "\n";
                
                return FALSE;
                
            }
            
            return $type_meta_arr;
            
        }
        
        protected function setTypeSpecificAttributes( $type_meta_arr )
        {
            
            try{
                
                if( array_key_exists( "Quantity", $type_meta_arr ) )//Information to be stored comes from DB
                {
                    
                    $this->weight = $type_meta_arr["Quantity"];
                    
                }
                else//Information to be stored comes from Add Product form
                {
                    
                    $this->weight = $type_meta_arr["Weight"];
                    
                }
                
            }catch( Exception $e ){
                
                echo "ERROR: Type specific attribute assignment failed\n" . $e->getMessage() . "\n";
                
                return FALSE;
                
            }
            
            return TRUE;
            
        }
        
        
    }

?>