<?php
namespace public_html\Model\ProductTypes;
    
    
    
    class FurnitureProduct extends \public_html\Model\AbstractProduct
    {
        
        protected $id;
        protected $sku;
        protected $name;
        protected $price;
        
        private $height;
        private $width;
        private $length;
        
        
        
        public function __construct( $furniture_meta_arr )
        {
            
            $this->setBaseAttributes( $furniture_meta_arr );
            $this->setTypeSpecificAttributes( $furniture_meta_arr );
            
        }
        
        protected function getTypeSpecificAttributes()
        {
            try{
                
                $type_meta_arr = [
                
                "Label"     =>  "Dimensions",
                
                "Quantity"  =>  $this->height . "x" . $this->width . "x" . $this->length,
                
                "Unit"      =>  "cm"
                
                ];
                
            }catch(Exception $e){
                
                echo "ERROR: Type specific attribute extraction failed" . $e->getMessage() . "\n";
                
                return FALSE;
                
            }
            
            return $type_meta_arr;
            
        }
        
        protected function setTypeSpecificAttributes( $type_meta_arr )
        {
            
            try{
                
                if( array_key_exists( "Quantity", $type_meta_arr ) )//Information to be stored comes from DB
                {
                    
                    $volumetric_quantity_string = trim( $type_meta_arr["Quantity"], " " );
                    $dimensions_arr = explode( "x", $volumetric_quantity_string);
                    
                    $this->height = floatval( $dimensions_arr[0] );
                    $this->width  = floatval( $dimensions_arr[1] );
                    $this->length = floatval( $dimensions_arr[2] ); 
                    
                }
                else//Information to be stored comes from Add Product form
                {
                    
                    $this->height = floatval( $type_meta_arr["Height"] );
                    $this->width  = floatval( $type_meta_arr["Width"] );
                    $this->length = floatval( $type_meta_arr["Length"] ); 
                    
                }
                
                
            }catch( Exception $e ){
                
                echo "ERROR: Type specific attribute assignment failed\n" . $e->getMessage() . "\n";
                
                return FALSE;
                
            }
            
            return TRUE;
            
        }
        
        
    }

?>