<?php
namespace public_html\Model;
    
    
    
    abstract class AbstractProduct
    {
        
        //Should return array
        protected abstract function getTypeSpecificAttributes(); 
        
        //Should return bool
        protected abstract function setTypeSpecificAttributes( $type_meta_arr );
        
        
        
        protected function getBaseAttributes()
        {
            
            try{
                
                $base_meta_arr = [
                    
                    "ID"    =>  $this->id,
                    
                    "SKU"   =>  $this->sku,
                    
                    "Name"  =>  $this->name,
                    
                    "Price" =>  $this->price
                    
                    ];
                
            }catch( Exception $e ){
                
                echo "ERROR: Base attribute extraction failed\n" . $e->getMessage() . "\n";
                
                return FALSE;
                
            }
            
            return $base_meta_arr;
            
        }
        
        protected function setBaseAttributes( $base_meta_arr )
        {
            
            try{
                
                //In the use case of saving a product the constructor doesn't need the product ID
                if( array_key_exists( "ID", $base_meta_arr ) )
                {
                    
                    $this->id = $base_meta_arr["ID"];
                    
                }
                else
                {
                    
                    $this->id = FALSE;
                    
                }
                
                $this->sku      =   $base_meta_arr["SKU"];
                $this->name     =   $base_meta_arr["Name"];
                $this->price    =   $base_meta_arr["Price"];
                
            }catch( Exception $e ){
                
                echo "ERROR: Base attribute assignment failed\n" . $e->getMessage() . "\n";
                
                return FALSE;
                
            }
            
            return TRUE;
      
        }
        
        public function getFullAttributeArray()
        {
            
            $full_attribute_arr = [
                
                "Base_Attributes"           =>  $this->getBaseAttributes(),
                
                "Type_Specific_Attributes"  =>  $this->getTypeSpecificAttributes()
                
                ];
                
            return $full_attribute_arr;
            
        }
        
    }