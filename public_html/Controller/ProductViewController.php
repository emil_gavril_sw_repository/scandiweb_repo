<?php
namespace public_html\Controller;



    class ProductViewController
    {
        
        protected static $type_specific_form_view_path_arr = [
            
            "CompactDiskProduct"    =>  "",
            
            "FurnitureProduct"      =>  "",
            
            "BookProduct"           =>  ""
            
            ];
        
        
        
        public function __construct()
        {
            
            $this->initTypeSpecificFormPathArr();
            
        }
        
        protected static function initTypeSpecificFormPathArr()
        {
            
            $TYPE_SPECIFIC_FORM_VIEW_BASE_PATH = realpath($_SERVER["DOCUMENT_ROOT"]) . 
    
                "/View/AddProductPage/TypeSpecificAttributeForms/";
            
            //Get full path of each product type specific form view and push them to the class static array
            $furniture_form_path = strval( $TYPE_SPECIFIC_FORM_VIEW_BASE_PATH . "furnitureTypeAttributeForm.html" );
            $book_form_path      = strval( $TYPE_SPECIFIC_FORM_VIEW_BASE_PATH . "bookTypeAttributeForm.html" );
            $cd_form_path        = strval( $TYPE_SPECIFIC_FORM_VIEW_BASE_PATH . "compactDiskTypeAttributeForm.html" );
                
            self::$type_specific_form_view_path_arr["CompactDiskProduct"] = $cd_form_path;
            self::$type_specific_form_view_path_arr["FurnitureProduct"]   = $furniture_form_path;
            self::$type_specific_form_view_path_arr["BookProduct"]        = $book_form_path;
            
        }
        
        public function getTypeSpecificFormPath( $index )
        {
            
            return self::$type_specific_form_view_path_arr[$index];
            
        }
        
        public function printOutData( $product_obj_arr )
        {
            if( $product_obj_arr != FALSE )
            {
                
                echo "<div class='container'>";
            
                $printout_index = 0;
                
                foreach( $product_obj_arr as $product_obj)
                {
                    
                    if( $printout_index % 4 == 0 )
                    {
                        
                        echo "<div class='d-flex justify-content-around'>";
                        
                    }
                    
                    $product_full_meta_arr = $product_obj->getFullAttributeArray();
                    
                    //Get and print product base attributes
                    $base_meta_arr = $product_full_meta_arr["Base_Attributes"];
                    
                        echo "
                                <div class='col-2 border rounded justify-content-center'> 
                                
                                    <div class='row'>
                                        <input class='delete-checkbox' type='checkbox' value='{$base_meta_arr['ID']}' id='delete-checkbox' style='margin-left: 12px; margin-top: 12px;'>
                                    </div>
                                    
                                    <div class='row justify-content-center'>
                                        <div class='p-2'><b>SKU:</b><br> 
                                        {$base_meta_arr['SKU']}</div> 
                                    </div>
                                    
                                    <div class='row justify-content-center'>
                                        <div class='p-2'><b>Name:</b><br>
                                        {$base_meta_arr['Name']}</div>
                                    </div>
                                    
                                    <div class='row justify-content-center'>
                                        <div class='p-2'><b>Price:</b><br>
                                        {$base_meta_arr['Price']} $</div>
                                    </div>
                               ";
                        
                        
                    //Get and print product type specific attributes    
                    $type_specific_attribute_arr = $product_full_meta_arr["Type_Specific_Attributes"];
                    
                    echo " <div class='row justify-content-center'>     
                                <div class='p-2'>
                    
                                    <b>{$type_specific_attribute_arr['Label']}:</b><br>
                                    {$type_specific_attribute_arr['Quantity']} {$type_specific_attribute_arr['Unit']}
                                
                                </div>
                            </div>
                        </div>";
                    
                    
                    if( $printout_index % 4 == 3 )
                    {
                        
                        echo "</div><br>";
                        
                    }
                    
                    $printout_index++;
                    
                }
              
                echo "</div></div>";
                
            }
           
        }
        
    }
    
?>