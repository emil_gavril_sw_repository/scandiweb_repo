
window.onload = function()
{
    
    let current_href = window.location.href;
    let url = null;
   
    switch( current_href )
    {
        
        case "https://juniortest-emil-gavril.000webhostapp.com/":
            
            url = "https://juniortest-emil-gavril.000webhostapp.com/Controller/MainFunctionalitiesController/GetProductList.php";
            break;
            
        case "https://juniortest-emil-gavril.000webhostapp.com/add-product/":
            
            url = "https://juniortest-emil-gavril.000webhostapp.com/Controller/MainFunctionalitiesController/GetAddProductForm.php"; 
            break;
            
        default:
        
            console.log("No further action required");
        
    }
    
    if( url )
    {
        let xh_req = new XMLHttpRequest();
            
        xh_req.onload = function()
        {
                
            document.getElementById("main-container").innerHTML = xh_req.responseText;
                
        };
            
        xh_req.open( "GET", url, true );
        xh_req.send( null );
    
    }
    
};

function DeleteSelectedProducts()
{
    
    let flag_no_products_selected = true;
    
    let checkbox_arr = document.getElementsByClassName( "delete-checkbox" );
    let checked_id_query_substr = "";
    
    for( var checkbox of checkbox_arr )
    {
    
        if( checkbox.checked )
        {
            
            checked_id_query_substr += checkbox.value.toString() + " | ";
            
            flag_no_products_selected = false;
            
        }
        
    }
    
    if( flag_no_products_selected )
    {
        
        console.log("No products have been selected");
        
    }
    else
    {
        
        checked_id_query_substr = checked_id_query_substr.substring( 0, checked_id_query_substr.length - 3 );
        
        const url = "https://juniortest-emil-gavril.000webhostapp.com/Controller/MainFunctionalitiesController/DeleteProducts.php";
        xh_req = new XMLHttpRequest();
        
        xh_req.onload = function()
        {
            
            document.location.reload();
            
        };
        
        xh_req.open( "POST", url, true );
        xh_req.setRequestHeader( "Content-Type", "application/x-www-form-urlencoded" );
        
        xh_req.send( "id_list=" + checked_id_query_substr );
        
    }
    
}

function GoToAddProductPage()
{
    
    const url = "https://juniortest-emil-gavril.000webhostapp.com/add-product/";
    xh_req = new XMLHttpRequest();
    
    xh_req.onload = function()
    {
       
        document.open();
        
        document.write(xh_req.responseText);
        window.location.href = "https://juniortest-emil-gavril.000webhostapp.com/add-product/";
        
        document.close();
        
    };
    
    xh_req.open("GET", url, true);
    xh_req.send(null);
    
}

function ReturnToMainPage()
{
    
    const url = "https://juniortest-emil-gavril.000webhostapp.com/";
    xh_req = new XMLHttpRequest();
    
    xh_req.onload = function()
    {
       
        document.open();
        
        document.write(xh_req.responseText);
        window.location.href = "https://juniortest-emil-gavril.000webhostapp.com/";
        
        document.close();
        
    };
    
    xh_req.open("GET", url, true);
    xh_req.send(null);
    
    
}
    
function SaveProduct()
{
    
    let parameter_string        = "";
    let flag_insufficient_info  = false;
    
    let form_inputs = Array.from(document.getElementById("product_form").elements);
    
    for(let index = 0; index < form_inputs.length; index++ )
    {
        
        if( form_inputs[index].value == "" )
        {
            
            flag_insufficient_info = true;
            
            break;
            
        }
        
        
        let input_value = form_inputs[index].value;
        let input_name  = form_inputs[index].name;
        
        parameter_string += input_name + "=" + input_value + "&";
        
        if( index == form_inputs.length - 1 )
        {
            
            parameter_string = parameter_string.slice(0, -1);
            
        }
        
    }
    
    
    if( !flag_insufficient_info )
    {
        
        const url = "https://juniortest-emil-gavril.000webhostapp.com/Controller/MainFunctionalitiesController/SaveProduct.php";
        xh_req = new XMLHttpRequest();
        
        xh_req.onload = function()
        {
            
            ReturnToMainPage();
            
        };
    
        xh_req.open("POST", url, true);
        xh_req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        
        xh_req.send(parameter_string);
        
    }
    else
    {
        
        alert("The information provided was insuficient to save the product.\nPlease make sure to fill in all the fields before pressing the 'Save' button");
        
    }
    
}

function GetTypeSpecificFormElement()
{
    
    let form_element_type = document.getElementById("productType").value;
    
    const url = "https://juniortest-emil-gavril.000webhostapp.com/Controller/MainFunctionalitiesController/GetTypeSpecificFormElement.php";
    xh_req = new XMLHttpRequest();
    
    xh_req.onload = function()
    {
        
        document.getElementById("product-attribute").innerHTML = xh_req.responseText;
        
    };
    
    xh_req.open("POST", url, true);
    xh_req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    
    xh_req.send("attribute_type=" + form_element_type);
    
}