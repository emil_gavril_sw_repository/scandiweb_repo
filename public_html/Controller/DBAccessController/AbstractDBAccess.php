<?php
namespace public_html\Controller\DBAccessController;
    
    $BASE_PATH = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once $BASE_PATH . "/autoloader.php";


    abstract class AbstractDBAccess
    {
        
        protected function connectToDB()
        {
            
            $this->db_connection = new \mysqli( $this->host, $this->username, $this->password, $this->db_name );
            
            if( !empty( $this->connection->connect_error ) )
            {
                
                 die(  var_dump( $this->db_connection->connect_error ) );
               
                
            }
            else
            {
                
                if( !isset( $this->db_table_arr["Type"] ) )
                {
                    
                    $this->db_table_arr["Type"] = "BaseTable";
                    
                }
                
                $full_path_table_class_name = "\\public_html\\Controller\\DBAccessController\\TableTypes\\";
                
                $table_class_name = $this->db_table_arr["Type"];
                
                
                $query_string = "SELECT table_name FROM information_schema.tables
                                            WHERE table_schema = 'id17860970_jwdt_scandi'";
                                            
                $query_result = \mysqli_query( $this->db_connection, $query_string );
                
                
                
                while( $row = $query_result->fetch_assoc()  )
                {
                    $final_table_class_name = $full_path_table_class_name . $table_class_name;
                    
                    $arr_index = $row["table_name"];
                    
                    $this->db_table_arr[$arr_index] = new $final_table_class_name( $this->db_connection, $arr_index );
                    
                }
                
                return TRUE;
                //echo "Connection to database successfully created<br><br>";
                //echo "All tables loaded into database access class instance<br><br>";
               
            }
               
        }
        
        protected function disconnectFromDB()
        {
            
            if( empty( $this->db_connection ) )
            {
                
                die( "There is no connection to close. Please create a DB connection first!" );
                
            }
            else
            {
                
                mysqli_close( $this->db_connection );
                
                //Reset the table array
                $this->db_table_arr = [ "Type" => NULL ];
                
                return TRUE;
                //echo "Connection to database succesfully closed";
                
            }
            
        }
        
       protected function getTable( $table_name )
       {
           
           return $this->db_table_arr[$table_name];
           
       }
       
    }

?>