<?php
namespace public_html\Controller\DBAccessController\DBAccessTypes;

    class SWTestAssignmentProductDBAccess extends \public_html\Controller\DBAccessController\AbstractDBAccess
    {
        
        protected $host;
        protected $username;
        protected $password;
        protected $db_name;
        
        protected $db_connection;
        protected $db_table_arr;
       
        
        
        public function __construct( $h = "localhost", $un = "id17860970_emilgavrilroot", $pw = "GigiPedala69#", $db_n = "id17860970_jwdt_scandi" )
        {
            
            $this->host     = $h;
            $this->username = $un;
            $this->password = $pw;
            $this->db_name  = $db_n;
            
            $this->db_table_arr = [ "Type" => NULL ];
            
        }
        
        public function deleteProducts( $product_id_arr )
        {
            
            $this->connectToDB();
            
            
            
            //Get "Product_Base_Attributes" table object
            $base_attr_table_obj = $this->getTable("Product_Base_Attributes");
            $base_attr_table_obj->deleteItemsByID( $this->db_connection, $product_id_arr );
            
            
            //Get "Product_Type_Specific_Attributes" table object
            $type_attr_table_obj = $this->getTable("Product_Type_Specific_Attributes");
            $type_attr_table_obj->deleteItemsByColumnName( $this->db_connection, $product_id_arr, "PBAID" );
            
            
            
            $this->disconnectFromDB();
            
        }
        
        public function addProduct( $child_product_obj )
        {
            
            $attribute_arr = $child_product_obj->getFullAttributeArray();
           
            $this->connectToDB();
            
            
            
            //Get "Product_Base_Attributes" table object
            $base_attr_table_obj = $this->getTable("Product_Base_Attributes");
            
            //Get base attribute array of product to be added and save it into the "Product_Base_Attributes" table
            $base_attribute_arr = $attribute_arr["Base_Attributes"];
            $product_base_attribute_id = $base_attr_table_obj->addItem( $this->db_connection, $base_attribute_arr );
            
            
            
            //Get "Product_Type_Specific_Attributes" table object
            $type_attr_table_obj = $this->getTable("Product_Type_Specific_Attributes");
            
            //Get base attribute array of product to be added and save it into the "Product_Type_Specific_Attributes" table
            $type_attribute_arr["Quantity"]     = $attribute_arr["Type_Specific_Attributes"]["Quantity"];
            $type_attribute_arr["Class_Name"]   = "\\" . str_replace( "\\", "\\\\", get_class( $child_product_obj ) );
            $type_attribute_arr["PBAID"]         = $product_base_attribute_id;
            
            $type_attr_table_obj->addItem( $this->db_connection, $type_attribute_arr );
            
            
            
            $this->disconnectFromDB();
           
        }
        
        //TO BE REMEMBERED: Two tables have been used for the storage of product attributes (1 for base attributes, 1 for type specific attributes)
        public function getFullProductList()
        {
            
            $product_constructor_parameter_arr = [];
            
            $this->connectToDB();
            
            
            
            //Get whole content of "Product_Base_Attributes" table
            $base_attr_table_obj = $this->getTable("Product_Base_Attributes");
            $result_arr = $base_attr_table_obj->getFullItemList( $this->db_connection );
            
            if( $result_arr != NULL )
            {
            
                foreach( $result_arr as $row)
                {
                    //Allows calling array elements by product ID
                    $index = $row["ID"];
                    
                    $product_constructor_parameter_arr[$index] = [
                        
                        "ID"    =>  $row["ID"],
                        
                        "SKU"   =>  $row["SKU"],
                        
                        "Name"  =>  $row["Name"],
                        
                        "Price" =>  $row["Price"]
                        
                        ];
                    
                }
            
            
            
                //Get partial content of "Product_Type_Specific_Attributes" table (PRIMARY KEY not needed)
                $type_attr_table_obj = $this->getTable( "Product_Type_Specific_Attributes" );
                $result_arr = $type_attr_table_obj->getFullItemList( $this->db_connection );
                
                foreach( $result_arr as $row )
                {
                    
                    $index = $row["PBAID"];
                    
                    $product_constructor_parameter_arr[$index]["Quantity"] = $row["Quantity"];
                    $product_constructor_parameter_arr[$index]["ClassName"] = $row["ClassName"];
                    
                }
                
                
                
                $product_obj_arr = [];
                
                foreach( $product_constructor_parameter_arr as $parameter_arr )
                {
                    
                    $index = $parameter_arr["ID"];
                    
                    $product_class_name = $product_constructor_parameter_arr[$index]["ClassName"];
                    
                    $product_obj_arr[$index] = new $product_class_name( $parameter_arr );
                    
                }
                
                
                
                $this->disconnectFromDB();
                
                return $product_obj_arr;
                
            }
            else
            {
                
                echo "<br>
                
                        <div class='d-flex justify-content-center'>
                            There are no products to show, please come back later
                        </div>
                        
                    <br>";
                
                return FALSE;
                
            }
            
        }
        
    }

?>