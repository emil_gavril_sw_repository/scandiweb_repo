<?php
namespace public_html\Controller\DBAccessController\TableTypes;

    $BASE_PATH = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once $BASE_PATH . "/autoloader.php";
    
    class BaseTable
    {
        
        //By default the ID column name will always be the first element in $column_name_arr
        private $column_name_arr;
        private $table_name;
        
        public function __construct( $db_connection, $table_name )
        {
            
            $this->table_name = $table_name;
            
            $query_string = "SELECT column_name FROM information_schema.columns WHERE table_name='{$table_name}'";
            $query_result = \mysqli_query( $db_connection, $query_string );
            
            for( $column_name_index = 0; $row = \mysqli_fetch_array( $query_result, MYSQLI_BOTH ); $column_name_index++ )
            {
                
                $this->column_name_arr[$column_name_index] = $row["column_name"];
                
            }
            
        }
        
        public function deleteItemsByID( $db_connection, $item_id_arr )
        {
            
            $query_string = "DELETE FROM {$this->table_name} WHERE ";
            
            foreach( $item_id_arr as $item_id )
            {
                
                $query_string .= " ID='{$item_id}' OR ";
                
            }
            
            $query_string = substr( $query_string, 0, strlen( $query_string) - 3 );
           
            \mysqli_query( $db_connection, $query_string );
            
        }
        
        public function deleteItemsByColumnName( $db_connection, $item_id_arr, $given_column_name )
        {
            
            $flag_column_exists = FALSE;
            
            foreach( $this->column_name_arr as $column_name)
            {
                
               if( $given_column_name == $column_name )
               {
                   
                   $flag_column_exists = TRUE;
                   
                   break;
                   
               }
                
            }
            
            if( $flag_column_exists )
            {
                
                $query_string = "DELETE FROM {$this->table_name} WHERE ";
                
                foreach( $item_id_arr as $item_id )
                {
                    
                    $query_string .= " {$given_column_name}='{$item_id}' OR ";
                    
                }
                
                $query_string = substr( $query_string, 0, strlen( $query_string) - 3 );
                 
                \mysqli_query( $db_connection, $query_string );
                
            }
            else
            {
                
                die("The provided column name is invalid. Please provide one of the column names associated with the table calling this function");
                
            }
            
        }
        
        public function addItem( $db_connection, $attribute_to_add_arr )
        {
            
            $query_string = "INSERT INTO {$this->table_name} (";
            
            foreach( $this->column_name_arr as $column_name)
            {
                
               $query_string .= $column_name . ", ";
                
            }
            
            $query_string = substr( $query_string, 0, strlen( $query_string) - 2 ) . ") VALUES (NULL, ";
            
            
            
            foreach( $attribute_to_add_arr as $attribute )
            {
                
                if( $attribute != FALSE )
                {
                    
                    $query_string .= "'" . $attribute . "', ";
                    
                }
                
            }
            
            $query_string = substr( $query_string, 0, strlen( $query_string) - 2 ) . ");";
            
            //Insert item into table
            \mysqli_query( $db_connection, $query_string );
            
            
            //Get ID of item that was just inserted
            $query_string = "SELECT MAX(ID) as PBAID FROM `Product_Base_Attributes` WHERE 1";

            $query_result = \mysqli_query( $db_connection, $query_string, MYSQLI_STORE_RESULT );
            $result = \mysqli_fetch_array( $query_result, MYSQLI_BOTH );
            
            return $result["PBAID"];
            
        }
        
        public function getFullItemList( $db_connection )
        {
            
            $query_string = "SELECT * FROM {$this->table_name} ";
        
            $query_result = \mysqli_query( $db_connection, $query_string );
            
            $result_arr = NULL;
            
            for( $table_item_index = 0; $row = \mysqli_fetch_array( $query_result, MYSQLI_BOTH ); $table_item_index++ )
            {
                
                $result_arr[$table_item_index] = $row;
                
            }
            
            return $result_arr; 
            
        }
        
    }
    
?>