<?php
namespace public_html\Controller\MainFunctionalitiesController;

    $BASE_PATH = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once $BASE_PATH . "/autoloader.php";
    
    
    $a_sw_db_instance = new \public_html\Controller\DBAccessController\DBAccessTypes\SWTestAssignmentProductDBAccess();
    
    //Get full product object list from database
    $product_obj_arr = $a_sw_db_instance->getFullProductList();
    
    
    $a_view_ctrl_instance = new \public_html\Controller\ProductViewController();
    
    //Print product list data on page
    $a_view_ctrl_instance->printOutData( $product_obj_arr );
   
    
    
    //Include page footer
    require_once $BASE_PATH . "/View/footer.html";
    
?>