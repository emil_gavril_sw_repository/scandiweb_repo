<?php
namespace public_html\Controller\MainFunctionalitiesController;

    $BASE_PATH = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once $BASE_PATH . "/autoloader.php";
    
    
    //The request POST array contains the full parameter list for product object instantiation
    $class_name = "\\public_html\\Model\\ProductTypes\\" . $_POST["Class_Name"];
    $product_obj = new $class_name( $_POST );
    
    
    //Instantiate DBAccess object and pass the product object as parameter
    $a_sw_db_instance = new \public_html\Controller\DBAccessController\DBAccessTypes\SWTestAssignmentProductDBAccess();
    $a_sw_db_instance->addProduct( $product_obj );
    
?>