<?php
namespace public_html\Controller\MainFunctionalitiesController;

    $BASE_PATH = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once $BASE_PATH . "/autoloader.php";
    
    $form_path_index =  $_POST["attribute_type"];

    $a_view_ctrl_instance = new \public_html\Controller\ProductViewController();
    $path_string = $a_view_ctrl_instance->getTypeSpecificFormPath( $form_path_index );
    
    require_once $path_string;
    
?>