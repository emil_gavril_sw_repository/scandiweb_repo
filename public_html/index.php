<html>
    
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
       
        <script type="text/javascript" src="/Controller/AControllerAPI.js"></script>
    </head>
    
    <body>
        
        <?php  
            
            $BASE_PATH = realpath($_SERVER["DOCUMENT_ROOT"]);
            
            //Include page header
            require_once $BASE_PATH . "/View/ProductListPage/productListPageHeader.html";
        
        ?>
        
        <div class="container" id="main-container">
            
            
            <div class="d-flex justify-content-center">
            
                <img src="/View/ProductListPage/loading.gif" width="22%" height="22%">
                
            </div>
        </div>
        
    </body>
    
</html>



